// DOM Manipulation
/*
	The document refers to the whole webpage.
	To access specific object models from the document, we can use:

		document.querySelector('#txt-first-name')

		document.getElementById('txt-first-name')
		document.getElementsByClassName('txt-inputs')
		document.getElementsByTagName('input')
*/


// Access the objects needed
const txtFirstName = document.querySelector('#txt-first-name');
const txtLastName = document.querySelector('#txt-last-name');
const spanFullName = document.querySelector('#span-full-name');


// call out the variable
// addEventListener('method/event or action')
// event parameter is triggered, what will happen next?

txtFirstName.addEventListener('keyup', (event) => {
	// innerHTML displays/adds an object in the document to access the value from the first name.
	spanFullName.innerHTML = `${txtFirstName.value} ${txtLastName.value}`;
});


// Multiple listeners can also be assigned to the same event
txtFirstName.addEventListener('keyup', (e) => {
	console.log(e.target);
	console.log(e.target.value); //similar to the txtFirstName.value
});

//Mini Activity
//Listen to an event when the last name's input is changed
//either add another event listener or to create a function that will update the span-full-name  content

const updateFullName = () => {
	let firstName = txtFirstName.value;
	let lastName = txtLastName.value;

	spanFullName.innerHTML = `${firstName} ${lastName}`;
};

txtFirstName.addEventListener('keyup', updateFullName);
txtLastName.addEventListener('keyup', updateFullName);